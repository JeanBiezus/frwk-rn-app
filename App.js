import React from 'react';
import { StatusBar } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Posts, Albums, Todos, LoadData } from './src/pages';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMessage, faImages, faClipboardList } from '@fortawesome/free-solid-svg-icons';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const tabPages = [
  { name: 'Posts', title: 'Posts', icon: faMessage, component: Posts },
  { name: 'Albums', title: 'Álbuns', icon: faImages, component: Albums },
  { name: 'Todos', title: 'Afazeres', icon: faClipboardList, component: Todos },
];

const screenOptions = ({ route }) => ({
  tabBarIcon: (iconProps) => <FontAwesomeIcon {...iconProps} icon={tabPages.find(p => p.name === route.name).icon} />
});

const TabNavigator = () => (
  <Tab.Navigator screenOptions={screenOptions}>
    {tabPages.map(({ title, ...pageProps }, index) => (
      <Tab.Screen options={{ title }} {...pageProps} key={index.toString()} />  
    ))}
  </Tab.Navigator>
)

const stackPages = [
  { name: 'LoadData', options: { title: null, headerTransparent: true }, component: LoadData },
  { name: 'TabNavigator', options: { header: () => {} }, component: TabNavigator },
];

const App = () => {
  return (
    <>
      <StatusBar backgroundColor='#f2f2f2' barStyle='dark-content'/>
      <NavigationContainer>
        <Stack.Navigator>
          {stackPages.map((page, index) => (
            <Stack.Screen {...page} key={index.toString()} />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
