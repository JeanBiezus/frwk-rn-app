describe('Abrir aplicação na página inicial', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('Deve abrir a tela "Posts"', async () => {
    await expect(element(by.id('pages/posts'))).toBeVisible();
  });
});
