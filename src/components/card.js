import React from 'react';
import { StyleSheet, View } from 'react-native';

const Card = ({ children, style, ...rest }) => {
    return (
        <View style={[styles.card, style]} {...rest}>
            {children}
        </View>
    )
};

const styles = StyleSheet.create({
    card: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 1,
        shadowRadius: 4,
        elevation: 4
    }
});

export default Card;