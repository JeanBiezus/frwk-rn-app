import React from 'react';
import { StyleSheet, View } from 'react-native';

const Divider = ({ style, ...rest }) => {
    return (
        <View style={[styles.divider, style]} {...rest}></View>
    );
};

const styles = StyleSheet.create({
    divider: {
        backgroundColor: '#ddd',
        flex: 1,
        height: 1,
        maxHeight: 1,
        marginVertical: 10
    }
});

export default Divider;