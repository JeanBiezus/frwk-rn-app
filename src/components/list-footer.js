import React from 'react';
import { ActivityIndicator, StyleSheet, View, Text } from 'react-native';

const ListFooter = ({ isLoading, isListEmpty, listEmptyText = 'Nenhum item encontrado!' }) => {

    if (!isLoading) return null;
    
    if (!isLoading && isListEmpty) return <Text style={styles.listEmptyText}>{listEmptyText}</Text>;

    return (
        <View style={styles.contentWrapper}>
            <ActivityIndicator size='large' />
        </View>
    );
};

const styles = StyleSheet.create({
    contentWrapper: {
        flex: 1,
        padding: 20,
        alignItems: 'center'
    },
    listEmptyText: {
        fontStyle: 'italic'
    }
});

export default ListFooter;