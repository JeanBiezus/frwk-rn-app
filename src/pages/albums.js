import React, { useEffect, useState } from 'react';
import { Text, FlatList, StyleSheet, View, Dimensions } from 'react-native';

import { faImages } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import Card from '../components/card';
import ListFooter from '../components/list-footer';

import { connectDB } from '../services/local-db';
import Divider from '../components/divider';

const Albums = () => {

    const [ albums, setAlbums ] = useState([]);
    const [ isLoadingALbums, setIsLoadingAlbums ] = useState(false);
    const [ isAlbumsOver, setIsAlbumsOver ] = useState(false);

    useEffect(() => {
        getAlbums();
    }, []);

    function getAlbums() {
        if (isAlbumsOver) return;

        setIsLoadingAlbums(true);

        let dbInstance;
        const limit = 12;
        connectDB()
        .then(async (db) => {
            dbInstance = db;
            return db.getAlbums({ limit, skip: albums.length });
        })
        .then((result) => {
            setAlbums(albums.concat(result));
            setIsAlbumsOver(result.length < limit);
        })
        .finally(() => {
            setIsLoadingAlbums(false);
        });
    }

    function renderAlbum(album) {
        return (
            <Card style={styles.card}>
                <View style={styles.albumContentContainer}>
                    <FontAwesomeIcon icon={faImages} style={styles.imagesIcon} size={styles.imagesIcon.fontSize} />
                    <Text style={styles.albumTitle}>{album.title}</Text>
                </View>
                <Divider />
                <View style={styles.userContentContainer}>
                    <Text>de</Text>
                    <Text style={styles.userName}>{album.user.name}</Text>
                </View>
            </Card>
        );
    }

    return (
        <FlatList
            data={albums}
            numColumns={2}
            renderItem={({ item }) => renderAlbum(item)}
            keyExtractor={item => item.id}
            ListFooterComponent={<ListFooter isLoading={isLoadingALbums} isListEmpty={!albums.length} />}
            contentContainerStyle={styles.contentContainer}
            onEndReached={() => getAlbums()}
            onEndReachedThreshold={0.1}
        />
    );
};


const { width: screenWidth } = Dimensions.get('screen');
const cardMargin = 8;
const cardWidth = (screenWidth / 2) - ((cardMargin * 2) + cardMargin); // Metade da tela - ((margens do card) + padding da tela)

const styles = StyleSheet.create({
    contentContainer: {
        padding: cardMargin,
        paddingBottom: 100
    },
    card: {
        margin: cardMargin,
        flexGrow: 1,
        maxWidth: cardWidth,
        minWidth: cardWidth
    },
    albumContentContainer: {
        alignItems: 'center',
        flexGrow: 1
    },
    albumTitle: {
        padding: 10,
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'center',
        flexGrow: 1
    },
    imagesIcon: {
        fontSize: 30,
        opacity: 0.6
    },
    userContentContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    userName: {
        marginLeft: 5,
        fontWeight: 'bold'
    }
});

export default Albums;