import Posts from './posts';
import Albums from './albums';
import Todos from './todos';
import LoadData from './load-data';

export {
    Posts,
    Albums,
    Todos,
    LoadData
}