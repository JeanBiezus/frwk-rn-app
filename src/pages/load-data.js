import React, { useEffect, useState } from "react";

import { View, Text, StyleSheet, ActivityIndicator } from "react-native";

import { connectDB } from '../services/local-db';

const LoadData = ({ navigation }) => {

    const [ progressInfoText, setProgressInfoText ] = useState();

    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        setProgressInfoText('Buscando informações...');

        let dbInstance;
        
        await new Promise(async (resolve) => {
            dbInstance = await connectDB();
            await dbInstance.setupDB();
            const posts = await dbInstance.getPosts({ limit: 1 });
            resolve(!!posts.length);
        })
        .then(isDBPopulated => {
            if (isDBPopulated) return navigation.replace('TabNavigator');
            
            const baseURL = 'https://jsonplaceholder.typicode.com';

            return Promise.all([
                fetch(`${baseURL}/users`),
                fetch(`${baseURL}/posts`),
                fetch(`${baseURL}/albums`),
                fetch(`${baseURL}/todos`)
            ]);
        })
        .then((responses) => {
            if (responses) return Promise.all(responses.map(r => r.json()));
        })
        .then(async (result) => {
            if (!result) return;

            const [users, posts, albums, todos] = result;

            setProgressInfoText('Preparando tudo...');

            await dbInstance.addUsers(users);
            await dbInstance.addPosts(posts);
            await dbInstance.addAlbums(albums);
            await dbInstance.addTodos(todos);

            navigation.replace('TabNavigator');
        })
        .catch((error) => {
            // TODO: mostrar mensagem de falha e apresentar
            // layout para tentar novamente
            console.log(error);
        });
    }

    return (
        <View style={styles.contentContainer} testID='pages/load-data'>
            <ActivityIndicator size="large" />
            <Text style={styles.loadingText}>{progressInfoText}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    contentContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loadingText: {
        margin: 20
    }
});

export default LoadData;