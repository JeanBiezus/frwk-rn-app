import React, { useEffect, useState } from 'react';
import { Text, FlatList, StyleSheet, View } from 'react-native';

import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import Card from '../components/card';
import ListFooter from '../components/list-footer';

import { connectDB } from '../services/local-db';
import Divider from '../components/divider';

const Posts = () => {

    const [ posts, setPosts ] = useState([]);
    const [ isLoadingPosts, setIsLoadingPosts ] = useState(false);
    const [ isPostsOver, setIsPostsOver ] = useState(false);

    useEffect(() => {
        getPosts();
    }, []);

    function getPosts() {
        if (isPostsOver) return;

        setIsLoadingPosts(true);

        let dbInstance;
        const limit = 6;
        connectDB()
        .then(async (db) => {
            dbInstance = db;
            return db.getPosts({ limit, skip: posts.length });
        })
        .then((result) => {
            setPosts(posts.concat(result));
            setIsPostsOver(result.length < limit);
        })
        .finally(() => {
            setIsLoadingPosts(false);
        });
    }

    function renderPost(post) {
        return (
            <Card style={styles.card}>
                <View style={styles.postContentContainer}>
                    <FontAwesomeIcon icon={faQuoteLeft} style={styles.quoteIcon} size={styles.quoteIcon.fontSize} />
                    <View style={styles.postContentWrapper}>
                        <Text style={styles.postTitle}>{post.title}</Text>
                        <Text style={styles.postBody}>{post.body}</Text>
                        <Divider />
                        <View style={styles.userContentContainer}>
                            <Text>~</Text>
                            <Text style={styles.userName}>{post.user.name}</Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }

    return (
        <FlatList
            data={posts}
            renderItem={({ item }) => renderPost(item)}
            keyExtractor={item => item.id}
            ListFooterComponent={<ListFooter isLoading={isLoadingPosts} isListEmpty={!posts.length}  />}
            contentContainerStyle={styles.contentContainer}
            onEndReached={() => getPosts()}
            onEndReachedThreshold={0.1}
            testID='pages/posts'
        />
    );
};

const styles = StyleSheet.create({
    contentContainer: {
        padding: 16,
        paddingBottom: 100
    },
    card: {
        marginVertical: 8
    },
    postTitle: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    postBody: {
        fontSize: 15,
        fontStyle: 'italic'
    },
    postContentContainer: {
        flexDirection: 'row'
    },
    quoteIcon: {
        fontSize: 30,
        opacity: 0.6
    },
    postContentWrapper: {
        flex: 1,
        marginLeft: 20
    },
    userContentContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end'
    },
    userName: {
        marginLeft: 5,
        fontWeight: 'bold'
    }
});

export default Posts;