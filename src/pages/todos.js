import React, { useEffect, useState } from 'react';
import { Text, FlatList, StyleSheet, View } from 'react-native';

import { faCircleCheck } from '@fortawesome/free-solid-svg-icons';
import { faCircle } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import Card from '../components/card';
import ListFooter from '../components/list-footer';

import { connectDB } from '../services/local-db';
import Divider from '../components/divider';

const Todos = () => {

    const [ todos, setTodos ] = useState([]);
    const [ isLoadingTodos, setIsLoadingTodos ] = useState(false);
    const [ isTodosOver, setIsTodosOver ] = useState(false);

    useEffect(() => {
        getTodos();
    }, []);

    function getTodos() {
        if (isTodosOver) return;

        setIsLoadingTodos(true);

        let dbInstance;
        const limit = 10;
        connectDB()
        .then(async (db) => {
            dbInstance = db;
            return db.getTodos({ limit, skip: todos.length });
        })
        .then((result) => {
            setTodos(todos.concat(result));
            setIsTodosOver(result.length < limit);
        })
        .finally(() => {
            setIsLoadingTodos(false);
        });
    }

    function renderTodo(todo) {
        return (
            <Card style={styles.card}>
                <View style={styles.todoContentContainer}>
                    <FontAwesomeIcon 
                        icon={todo.completed ? faCircleCheck : faCircle }
                        style={[styles.todoIcon, todo.completed ? styles.completedTodoIcon : {}]} 
                        size={styles.todoIcon.fontSize} 
                    />
                    <View style={styles.todoContentWrapper}>
                        <Text style={styles.todoTitle}>{todo.title}</Text>
                        <View style={styles.userContentContainer}>
                            <Text style={styles.usersTodoText}>Tarefa de</Text>
                            <Text style={styles.userName}>{todo.user.name}</Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }

    return (
        <FlatList
            data={todos}
            renderItem={({ item }) => renderTodo(item)}
            keyExtractor={item => item.id}
            ListFooterComponent={<ListFooter isLoading={isLoadingTodos} isListEmpty={!todos.length} />}
            contentContainerStyle={styles.contentContainer}
            onEndReached={() => getTodos()}
            onEndReachedThreshold={0.1}
        />
    );
};

const styles = StyleSheet.create({
    contentContainer: {
        padding: 16,
        paddingBottom: 100
    },
    card: {
        marginVertical: 8
    },
    todoTitle: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    todoContentContainer: {
        flexDirection: 'row'
    },
    todoIcon: {
        fontSize: 30,
        color: 'grey',
        opacity: 0.6
    },
    completedTodoIcon: {
        color: 'green'
    },
    todoContentWrapper: {
        flex: 1,
        marginLeft: 20
    },
    userContentContainer: {
        flexDirection: 'row',
        flex: 1
    },
    usersTodoText: {
        fontStyle: 'italic'
    },
    userName: {
        fontWeight: 'bold',
        marginLeft: 5,
        fontStyle: 'italic'
    }
});

export default Todos;