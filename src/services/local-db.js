import { openDatabase, enablePromise } from 'react-native-sqlite-storage';

export const connectDB = async () => {
    enablePromise(true);
    return openDatabase({name: 'frwkrnapp.db', location: 'default'})
        .then(dbInstance => ({
            instance: dbInstance,
            setupDB: () => setupDB(dbInstance),
            getPosts: (filters) => getPosts(dbInstance, filters),
            getAlbums: (filters) => getAlbums(dbInstance, filters),
            getTodos: (filters) => getTodos(dbInstance, filters),
            addUsers: (users) => addUsers(dbInstance, users),
            addPosts: (posts) => addPosts(dbInstance, posts),
            addAlbums: (albums) => addAlbums(dbInstance, albums),
            addTodos: (todos) => addTodos(dbInstance, todos)
        }));
};

const setupDB = async (dbInstance) => {
    const createUserTableSQL = `
        CREATE TABLE IF NOT EXISTS user_info (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL
        );
    `;

    const createPostTableSQL = `
        CREATE TABLE IF NOT EXISTS post (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            body TEXT NOT NULL,
            user_id INTEGER NOT NULL,
            FOREIGN KEY (user_id) 
                REFERENCES user_info (user_id) 
                    ON DELETE CASCADE 
                    ON UPDATE NO ACTION
        );
    `;

    const createAlbumTableSQL = `
        CREATE TABLE IF NOT EXISTS album (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            user_id INTEGER NOT NULL,
            FOREIGN KEY (user_id) 
                REFERENCES user_info (user_id) 
                    ON DELETE CASCADE 
                    ON UPDATE NO ACTION
        );
    `;

    const createTodoTableSQL = `
        CREATE TABLE IF NOT EXISTS todo (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            completed BOOLEAN NOT NULL,
            user_id INTEGER NOT NULL,
            FOREIGN KEY (user_id) 
                REFERENCES user_info (user_id) 
                    ON DELETE CASCADE 
                    ON UPDATE NO ACTION
        );
    `;

    await dbInstance.executeSql(createUserTableSQL);
    await dbInstance.executeSql(createPostTableSQL);
    await dbInstance.executeSql(createAlbumTableSQL);
    await dbInstance.executeSql(createTodoTableSQL);
}

const getPosts = async (dbInstance, filters = {}) => {
    const select = `
        SELECT
            p.id as "id",
            p.title as "title",
            p.body as "body",
            ui.id as "userId",
            ui.name as "userName"
        FROM post p
        INNER JOIN user_info ui ON
            ui.id = p.user_id
        LIMIT ${filters.limit || 0}
        OFFSET ${filters.skip || 0}
    `;

    const results = await dbInstance.executeSql(select);
    const posts = [];

    results.forEach(result => {
        for (let i = 0; i < result.rows.length; i++) {
            const postRow = result.rows.item(i);
            posts.push({
                id: postRow.id,
                title: postRow.title,
                body: postRow.body,
                user: {
                    id: postRow.userId,
                    name: postRow.userName
                }
            });       
        }
    });

    return posts;
};

const getAlbums = async (dbInstance, filters = {}) => {
    const select = `
        SELECT
            a.id as "id",
            a.title as "title",
            ui.id as "userId",
            ui.name as "userName"
        FROM album a
        INNER JOIN user_info ui ON
            ui.id = a.user_id
        LIMIT ${filters.limit || 0}
        OFFSET ${filters.skip || 0}
    `;

    const results = await dbInstance.executeSql(select);
    const albums = [];

    results.forEach(result => {
        for (let i = 0; i < result.rows.length; i++) {
            const albumRow = result.rows.item(i);
            albums.push({
                id: albumRow.id,
                title: albumRow.title,
                user: {
                    id: albumRow.userId,
                    name: albumRow.userName
                }
            });       
        }
    });

    return albums;
};

const getTodos = async (dbInstance, filters = {}) => {
    const select = `
        SELECT
            t.id as "id",
            t.title as "title",
            t.completed as "completed",
            ui.id as "userId",
            ui.name as "userName"
        FROM todo t
        INNER JOIN user_info ui ON
            ui.id = t.user_id
        LIMIT ${filters.limit || 0}
        OFFSET ${filters.skip || 0}
    `;

    const results = await dbInstance.executeSql(select);
    const todos = [];

    results.forEach(result => {
        for (let i = 0; i < result.rows.length; i++) {
            const todoRow = result.rows.item(i);
            todos.push({
                id: todoRow.id,
                title: todoRow.title,
                completed: todoRow.completed,
                user: {
                    id: todoRow.userId,
                    name: todoRow.userName
                }
            });       
        }
    });

    return todos;
};

const addUsers = async (dbInstance, users) => {
    if (!users.length) return;

    const insert = `
        INSERT INTO user_info (id, name)
        VALUES
            ${users.map(u => `(${u.id}, "${u.name}")`)}
    `;

    await dbInstance.executeSql(insert);
};

const addPosts = async (dbInstance, posts) => {
    if (!posts.length) return;

    const insert = `
        INSERT INTO post (id, title, body, user_id)
        VALUES
            ${posts.map(p => `(${p.id}, "${p.title}", "${p.body}", ${p.userId})`)}
    `;

    await dbInstance.executeSql(insert);
};

const addAlbums = async (dbInstance, albums) => {
    if (!albums.length) return;

    const insert = `
        INSERT INTO album (id, title, user_id)
        VALUES
            ${albums.map(a => `(${a.id}, "${a.title}", ${a.userId})`)}
    `;

    await dbInstance.executeSql(insert);
};

const addTodos = async (dbInstance, todos) => {
    if (!todos.length) return;

    const insert = `
        INSERT INTO todo (id, title, completed, user_id)
        VALUES
            ${todos.map(t => `(${t.id}, "${t.title}", ${t.completed}, ${t.userId})`)}
    `;

    await dbInstance.executeSql(insert);
}